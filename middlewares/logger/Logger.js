const fs = require('fs');
const Logger = {

    formatDate: function(date, file=false)
    {
        let day = date.getDay() < 10 ? '0' + date.getDay() : date.getDay();
        let month = date.getMonth() + 1 < 10 ? '0' + date.getMonth() + 1 : date.getMonth() + 1;

        let hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
        let minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
        let seconds = date.getSeconds() < 10 ? '0' + date.getSeconds(): date.getSeconds();

        if (file){
            return date.getFullYear() + "-" + month + "-" + day;
        } else {
            return date.getFullYear() + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        }
    },

    getFile: function()
    {
        return "log_" + Logger.formatDate(new Date(), true) + ".txt";
    },

    writeInLog: function(message)
    {
        fs.writeFile("logs/" + this.getFile(), "[" + this.formatDate(new Date()) + "] " + message + "\n", { flag: 'a+' }, (err) => {});
    }
}

module.exports = Logger;