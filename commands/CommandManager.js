const { SlashCommandBuilder } = require("@discordjs/builders");
const { clientId, guildId, token } = require('../config.json');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

const PingCommand = require("./PingCommand");
const { Client } = require("discord.js");

class CommandManager {

    /** @type {Client} */
    client = null;

    /** @type {REST} */
    rest = null;

    commands = [
        new PingCommand(this.client),
        //Add your new command object here!
    ];

    registeredCommands = [];

    constructor(client) {
        this.client = client;
        this.rest = new REST({ version: '9' }).setToken(token);
        
        //Register commands
        this.commands.forEach((command) => {
            this.registeredCommands.push(new SlashCommandBuilder().setName(command.name).setDescription(command.description).toJSON());
        });

        this.client.on('interactionCreate', async interaction => {
            if (!interaction.isCommand()) return;
            
            const { commandName } = interaction;

            this.commands.forEach((command) => {
                if (commandName == command.name)
                {
                    command.execute(interaction);
                }
            });
        });

        this.rest.put(Routes.applicationGuildCommands(clientId, guildId), { body: this.registeredCommands })
        .then(() => console.log('Successfully registered application commands.'))
        .catch(console.error);

    }

    
 
}

module.exports = CommandManager;