const { Client } = require('discord.js');
const Logger = require('../middlewares/logger/Logger');

class Command {

    /** @type {Client} */
    client = null;

    /** @type {string} */
    name = null;

    /** @type {string} */
    description = null;

    /**
     * 
     * @param {Client} client 
     * @param {string} name 
     * @param {string} description 
     */
    constructor(client, name, description)
    {
        this.client = client;
        this.name = name;
        this.description = description;
    }

    execute(interaction) {
        Logger.writeInLog("Se ha ejecutado el comando /" + this.name);
    }
}

module.exports = Command;