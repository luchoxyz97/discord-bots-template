const { Client } = require("discord.js");
const Command = require("./Command");
class PingCommand extends Command {

    /**
     * 
     * @param {Client} client 
     */
    constructor(client){
        super(client, "ping", "Comando de ejemplo");
    }

    execute(interaction){
        super.execute(interaction);
        interaction.reply("Hola mundo!");
    }

}
module.exports = PingCommand;