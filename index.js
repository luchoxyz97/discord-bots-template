// Require the necessary discord.js classes
const { Client, Intents } = require('discord.js');
const CommandManager = require('./commands/CommandManager');
const { token } = require('./config.json');
const EventManager = require('./events/EventManager');

// Create a new client instance
const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

//Register Commands
var commandManager = new CommandManager(client);

//Register Events
var eventManager = new EventManager(client);

// When the client is ready, run this code (only once)
client.once('ready', () => {
	console.log('Plantilla de Bots de Discord iniciada!');
});

// Login to Discord with your client's token
client.login(token);