const TestEvent = require("./TestEvent");

class EventManager {
    client = null;
    events = null;

    registerEvents() {
        this.events.forEach((event) => {
            event.doIt();
        });
    }

    constructor(client) {
        this.client = client;

        //The events must be registered here!
        this.events = [
            new TestEvent(this.client)
        ];

        this.registerEvents();
    }

 
}

module.exports = EventManager;