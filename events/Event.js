const { Client } = require('discord.js');
const Logger = require('../middlewares/logger/Logger');
class Event {

    /** @type {Client} */
    client = null;

    /** @type {string} */
    eventName = null;

    /** @type {boolean} */
    isOnce = false;

    /**
     * 
     * @param {Client} client 
     * @param {string} eventName 
     * @param {boolean} isOnce 
     */
    constructor(client, eventName, isOnce)
    {
        this.client = client;
        this.eventName = eventName;
        this.isOnce = isOnce;
    }

    doIt(callback) {
        if (this.isOnce)
        {
        
            this.client.once(this.eventName, function(){
                Logger("Se ha ejecutado el evento " + this.eventName + " una vez.");
                callback(arguments);
            });
        } else {
            this.client.on(this.eventName, function(){
                Logger("Se ha ejecutado el evento " + this.eventName + " recurrente.");
                callback(arguments);
            });
        }
    }

}

module.exports = Event;