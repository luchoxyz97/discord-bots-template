const Event = require('./Event');
const { Client } = require('discord.js');
class TestEvent extends Event {

    /**
     * 
     * @param {Client} client 
     */
    constructor(client){
        super(client, "messageReactionAdd", true);
    }

    doIt() {
        console.log("doing");
        super.doIt(function(messageReaction, user) {  
            console.log(`a reaction is added to a message`);
        });
    }

}

module.exports = TestEvent;